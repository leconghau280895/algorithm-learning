  getUnableEatStudent(): void{
    let dem = 0;
    while(dem !== this.students.length){
      let student: any = this.students.shift();
      let sandwich: any = this.sandwiches.shift();

      if (student !== sandwich){
        this.students.push(student);
        this.sandwiches.push(sandwich);
        dem = dem + 1;
      }else{
        dem = 0;
      }
    }
  }